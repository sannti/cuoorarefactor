Class {
	#name : #PopularToday,
	#superclass : #QuestionRetriever,
	#category : #'TP-Refactoring-Model'
}

{ #category : #retrieving }
PopularToday >> retrieveQuestions: aUser [
	| popularTCol averageVotes temp qRet|
	popularTCol := cuoora newQuestions.
	averageVotes := cuoora averageVotes.
	temp := self collectionOrderByPositivesVotes: (popularTCol
		select: [ :q | q positiveVotes size >= averageVotes ]).
	qRet := temp last: (100 min: temp size).
	^ qRet reject: [ :q | q user = aUser ]

]
