Class {
	#name : #Publicacion,
	#superclass : #Object,
	#instVars : [
		'timestamp',
		'user',
		'votes',
		'description'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #adding }
Publicacion >> addVote: aVote [
	votes add: aVote
]

{ #category : #accessing }
Publicacion >> description [
	^ description
]

{ #category : #private }
Publicacion >> description: anObject [
	description := anObject
]

{ #category : #'as yet unclassified' }
Publicacion >> negativeVotes [
	| r |
	r := OrderedCollection new.
	r := votes reject:[:vote | vote isLike ].
	^r
]

{ #category : #'as yet unclassified' }
Publicacion >> positiveVotes [
	| r |
	r := OrderedCollection new.
	r:= votes select: [:vote | vote isLike ].
	^ r
]

{ #category : #accessing }
Publicacion >> timestamp [
	^ timestamp
]

{ #category : #private }
Publicacion >> timestamp: anObject [
	timestamp := anObject
]

{ #category : #accessing }
Publicacion >> user [
	^ user
]

{ #category : #private }
Publicacion >> user: anObject [
	user := anObject
]

{ #category : #accessing }
Publicacion >> votes [
	^ votes
]
