Class {
	#name : #Answer,
	#superclass : #Publicacion,
	#instVars : [
		'question'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Answer class >> new: anAnswer user: aUser question: aQuestion [
	^ self new
		initializeDescripcion: anAnswer
		user: aUser
		question: aQuestion
]

{ #category : #initialize }
Answer >> initialize [
	votes := OrderedCollection new.
	timestamp := DateAndTime now.
]

{ #category : #initialization }
Answer >> initializeDescripcion: anAnswer user: aUser question: aQuestion [
	description := anAnswer.
	user := aUser.
	question := aQuestion
]
