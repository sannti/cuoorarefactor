Class {
	#name : #Social,
	#superclass : #QuestionRetriever,
	#category : #'TP-Refactoring-Model'
}

{ #category : #retrieving }
Social >> retrieveQuestions: aUser [
    | qRet temp |
    temp := self collectionOrderByPositivesVotes:  aUser followersQuestions.
    qRet := temp last: (100 min: temp size).
    ^ qRet reject: [ :q | q user = aUser ]
]
