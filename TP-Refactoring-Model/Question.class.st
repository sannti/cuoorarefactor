Class {
	#name : #Question,
	#superclass : #Publicacion,
	#instVars : [
		'title',
		'answers',
		'topics'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Question class >> newWithTitle: title description: aDescription user: aUser [
	^ self new
		initializeTitle: title
		description: aDescription
		user: aUser

]

{ #category : #'instance creation' }
Question class >> newWithTitle: title description: aDescription user: aUser topic: aTopic [
	^ self new
		initializeTitle: title
		description: aDescription
		user: aUser
		topic: aTopic
]

{ #category : #adding }
Question >> addTopic: aTopic [
	topics add: aTopic.
	aTopic addQuestion: self.

]

{ #category : #'initalize-release' }
Question >> initialize [
	answers := OrderedCollection new.
	topics := OrderedCollection new.
	votes := OrderedCollection new.
	timestamp := DateAndTime now.
]

{ #category : #initialization }
Question >> initializeTitle: aTitle description: aDescription user: aUser [
	title := aTitle.
	description := aDescription.
	user := aUser
]

{ #category : #initialization }
Question >> initializeTitle: aTitle description: aDescription user: aUser topic: aTopic [
	title := aTitle.
	description := aDescription.
	user := aUser.
	self addTopic: aTopic
]

{ #category : #accessing }
Question >> title [
	^title 
]

{ #category : #accessing }
Question >> title: aTitle [
	title := aTitle 
]

{ #category : #accessing }
Question >> topics [
	^topics 
]
