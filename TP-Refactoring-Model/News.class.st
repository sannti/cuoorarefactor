Class {
	#name : #News,
	#superclass : #QuestionRetriever,
	#category : #'TP-Refactoring-Model'
}

{ #category : #retrieving }
News >> retrieveQuestions: aUser [
	| qRet temp |
	temp := self collectionOrderByPositivesVotes:  cuoora newQuestions.
	qRet := temp last: (100 min: temp size).
	^ qRet reject: [ :q | q user = aUser ]
]
