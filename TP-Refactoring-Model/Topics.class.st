Class {
	#name : #Topics,
	#superclass : #QuestionRetriever,
	#category : #'TP-Refactoring-Model'
}

{ #category : #retrieving }
Topics >> retrieveQuestions: aUser [
	| qRet temp |
    temp := self collectionOrderByPositivesVotes:  aUser topicsQuestions.
    qRet := temp last: (100 min: temp size).
    ^ qRet reject: [ :q | q user = aUser ]
	

]
