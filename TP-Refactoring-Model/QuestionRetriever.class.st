Class {
	#name : #QuestionRetriever,
	#superclass : #Object,
	#instVars : [
		'cuoora'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
QuestionRetriever class >> new: cuoora [
	^ self new cuoora: cuoora
]

{ #category : #accesing }
QuestionRetriever >> collectionOrderByPositivesVotes: aCollection [ 
 ^ aCollection asSortedCollection: [ :a :b | a positiveVotes size > b positiveVotes size ]
]

{ #category : #accesing }
QuestionRetriever >> cuoora: aCuooraInstance [
	cuoora := aCuooraInstance 
]

{ #category : #retrieving }
QuestionRetriever >> retrieveQuestions: aUser [
	^ self subclassResponsibility 
]
